//
//  StatisticsViewController.swift
//  Guessapp
//
//  Created by student on 2/26/19.
//  Copyright © 2019 student. All rights reserved.
//

import UIKit

class StatisticsViewController: UIViewController {
    
    
    @IBOutlet weak var minLBL: UILabel!
    
    @IBOutlet weak var maxLBL: UILabel!
    
    
    @IBOutlet weak var meanLBL: UILabel!
    
    @IBOutlet weak var stdDevLBL: UILabel!
    
    @IBAction func clearStatisticsBTN(_ sender: Any) {
       
        minLBL.text = "0"
        maxLBL.text = "0"
        meanLBL.text = "0"
        stdDevLBL.text = "0"
        Guesser.shared.clearStatistics()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        clearLabels()
    }
    func clearLabels(){
        if Guesser.shared.numGuesses() == 0 {
            meanLBL.text = "0.00"
            stdDevLBL.text = "0.00"
        }else{
        minLBL.text = String(Guesser.shared.minimumNumAttempts())
        maxLBL.text = String(Guesser.shared.maximumNumAttempts())
        var count = 0
        for i in 0..<Guesser.shared.numGuesses(){
            count = count+Guesser.shared.guess(index: i).numAttemptsRequired
        }
        let mean = Double(count)/Double(Guesser.shared.numGuesses())
            meanLBL.text = String(format: "%.2f", mean)
        
    var stdDevCount = 0.0
    for i in 0..<Guesser.shared.numGuesses(){
    stdDevCount += pow(Double(Guesser.shared.guess(index: i).numAttemptsRequired) - mean,2)
    }
            let const  = sqrt(stdDevCount/Double(Guesser.shared.numGuesses()))
stdDevLBL.text = String(format: "%.2f", const)

        }}
    
    
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
clearLabels()
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
